# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=5

PYTHON_COMPAT=( python{2_7,3_4,3_5,3_6} )

inherit distutils-r1

MY_PN="spglib"

DESCRIPTION="Python bindings for spglib library for finding and handling crystal symmetries"
HOMEPAGE="http://spglib.sourceforge.net/"
#SRC_URI="http://downloads.sourceforge.net/project/${PN}/${PN}/${PN}-${MY_PV}/${P}.tar.gz"
SRC_URI="mirror://sourceforge/${MY_PN}/${MY_PN}-${PV}.tar.gz"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64"

S="${WORKDIR}/${MY_PN}-${PV}/python/ase"

