# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=5

PYTHON_COMPAT=( python2_7 python3_3 python3_4 )

inherit distutils-r1

DESCRIPTION="libAtoms QUIP molecular dynamics framework"
HOMEPAGE="http://www.libatoms.org"

SRC_URI="mirror://pypi/${PN:0:1}/${PN}/${P}.tar.gz"
KEYWORDS="~amd64 ~x86"
if [[ ${PV} = *9999* ]]; then
	SRC_URI=
	KEYWORDS=
	EGIT_REPO_URI="git://github.com/libAtoms/QUIP.git
		https://github.com/libAtoms/QUIP.git"
	inherit git-r3
fi

LICENSE="BSD"
SLOT="0"

RDEPEND="
	dev-python/matplotlib[${PYTHON_USEDEP}]
	dev-python/jinja[${PYTHON_USEDEP}]"

src_prepare() {
	python setup.py submodule
}

