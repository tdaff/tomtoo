# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=5

PYTHON_COMPAT=( python{2_7,3_3,3_4} )

inherit distutils-r1

DESCRIPTION="Manage Remote and Local Kernels for Jupyter"
HOMEPAGE="https://bitbucket.org/tdaff/remote_ikernel"
SRC_URI="mirror://pypi/${PN:0:1}/${PN}/${P}.tar.gz"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64 ~x86 ~amd64-linux ~x86-linux"

RDEPEND="|| ( dev-python/ipython[${PYTHON_USEDEP},notebook] dev-python/notebook[$PYTHON_USEDEP] )
	dev-python/pexpect[${PYTHON_USEDEP}]
	www-servers/tornado[${PYTHON_USEDEP}]"

DOCS=( README.rst )

