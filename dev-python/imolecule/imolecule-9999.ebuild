# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=5

PYTHON_COMPAT=( python{2_7,3_4} )

inherit distutils-r1

DESCRIPTION="An embeddable webGL molecule viewer and file format converter."
HOMEPAGE="http://patrickfuller.github.io/imolecule/"
SRC_URI="mirror://pypi/${PN:0:1}/${PN}/${P}.tar.gz"
KEYWORDS="~amd64 ~x86"
if [[ ${PV} = *9999* ]]; then
	SRC_URI=
	KEYWORDS=""
	EGIT_REPO_URI="git://github.com/patrickfuller/imolecule.git
		https://github.com/patrickfuller/imolecule.git"
	inherit git-r3
fi

LICENSE="BSD"
SLOT="0"

RDEPEND="sci-chemistry/openbabel-python[$PYTHON_USEDEP]"

DOCS=( README.md )

src_prepare() {
	sed -i "s/\(.*__version__\)/#\1/" setup.py || die
}

