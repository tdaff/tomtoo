# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=5

PYTHON_COMPAT=( python{2_7,3_4,3_5,3_6} )

inherit distutils-r1

DESCRIPTION="Bringing Matplotlib to the Browser"
HOMEPAGE="http://mpld3.github.io"

SRC_URI="mirror://pypi/${PN:0:1}/${PN}/${P}.tar.gz"
KEYWORDS="~amd64 ~x86"
if [[ ${PV} = *9999* ]]; then
	SRC_URI=
	KEYWORDS="~amd64 ~x86"
	EGIT_REPO_URI="git://github.com/jakevdp/mpld3.git
		https://github.com/jakevdp/mpld3.git"
	inherit git-r3
fi

LICENSE="BSD"
SLOT="0"

RDEPEND="
	dev-python/matplotlib[${PYTHON_USEDEP}]
	dev-python/jinja[${PYTHON_USEDEP}]"

src_prepare() {
	python setup.py submodule
}

