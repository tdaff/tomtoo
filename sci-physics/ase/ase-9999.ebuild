# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

PYTHON_COMPAT=( python{2_7,3_4,3_5,3_6} )

inherit distutils-r1 eutils git-r3

if [ ${PV} == "9999" ] ; then
	EGIT_REPO_URI="https://gitlab.com/ase/ase.git"
	KEYWORDS="~amd64"
else
	EGIT_REPO_URI="https://gitlab.com/ase/ase.git"
	EGIT_COMMIT="refs/tags/${PV}"
	KEYWORDS="~amd64"
fi

DESCRIPTION="An Atomistic Simulation Environment written in the Python programming language"
HOMEPAGE="https://wiki.fysik.dtu.dk/ase/"

LICENSE="LGPL"
SLOT="0"
IUSE="doc"
DOCS=( README.rst )

RDEPEND="
	dev-python/numpy[${PYTHON_USEDEP}]"

DEPEND="
	${RDEPEND}
	doc? ( 
		dev-python/sphinx[${PYTHON_USEDEP}]
		media-gfx/imagemagick
		)
	"

src_prepare() {
	sed -i "s/name = 'python-ase'/name = 'ase'/" setup.py || die
}

python_compile_all() {
	if use doc; then
		python_setup
		python_export
		cd doc && \
			PYTHONPATH="${BUILD_DIR}"/lib \
			emake html
	fi
}

python_install_all() {
	use doc && HTML_DOCS=( doc/_build/html/. )
	distutils-r1_src_install_all
}

